import os
print "Python CWD:", os.getcwd()

import unreal_engine as ue
from sensornetwork.Communication.ServerUdp import ServerUdp
from sensornetwork.Server.SensorServer import SensorServer
import SocketServer
import json
import sys

notificationConditions = [
    {
        "deviceId": "RPI1",
        "sensorType": "dht22",
        "sensorId": 13,
        "dataCondition":
            {
                "temperature": [-10, 0],
                "humidity": [0.00, 1.00]
            }
    },
    {
        "deviceId": "RPI1",
        "sensorType": "slm393",
        "sensorId": 12,
        "dataCondition":
            {
                "rad": True
            }
    },
    {
        "deviceId": "RPI1",
        "sensorType": "stcs230",
        "sensorId": 2,
        "dataCondition":
            {
                "R": [0, 150],
                "G": [150, 255],
                "B": [0, 255]
            }

    },
    {
        "deviceId": "RPI1",
        "sensorType": "hcsr501",
        "sensorId": 2,
        "dataCondition":
            {
                "movement" : True
            }
    },
    {
        "deviceId": "RPI1",
        "sensorType": "hcsr04",
        "sensorId": 2,
        "dataCondition":
        {
            "distance" : [200, 400] #mjerenje udaljenosti u cm
        }
    },
    {
        "deviceId": "RPI1",
        "sensorType": "dht22",
        "sensorId": 13,
        "dataCondition":
        {
                "temperature": [10, 20],
                "humidity": [0.00, 1.00]
        }
    }
]


class SnServerComponent():

    # constructor adding a component
    def __init__(self):
        print "SnServerComponent Pythoninit"

    def begin_play(self):
        ue.log('SnServerComponent PythonBeginPlay')
        #defaultna vrijednost host i port parametra ako se ne zadaju prilikom pokretanja skripte
        HOST, PORT = "localhost", 9998
        if len(sys.argv) == 3:
            HOST, PORT = sys.argv[1], int(sys.argv[2])
        elif len(sys.argv) == 2:
            HOST = sys.argv[1]
            PORT = PORT

        #podaci potrebni za konekciju na bazu
        with open('databaseConnection.js') as data_file:
            data_file = json.load(data_file)

        hostname = data_file["hostname"]
        username = data_file["username"]
        password = data_file["password"]
        dbName = data_file["dbName"]
        storageType = data_file["storageType"]

        storageParameters = { "hostname":hostname, "user":username, "password":password, "dbName":dbName}
        #stvaranje senor server klase koja sluzi za obradu podataka i pokretanje UDP servera
        ServerUdp.sensorServer = SensorServer(storageType, storageParameters, notificationConditions)

        print "Start UDP Server"
        server = SocketServer.UDPServer((HOST, PORT), ServerUDP)
        #server.serve_forever()

    def tick(self, delta_time):
        #print 'PythonTick'
        pass
