import os
print "Python CWD2:", os.getcwd()

import socket
import sys
import json
import time
import thread

from sensornetwork.Sensors.SensorBase import SensorBase
from sensornetwork.Communication.ClientUdp import ClientUdp
from datetime import datetime

import unreal_engine as ue
from unreal_engine.classes import FloatProperty
#from unreal_engine.classes import FloatProperty, StringProperty
#from unreal_engine.classes import Character, PawnSensingComponent, Pawn, FloatProperty

class SnClientComponent():

    Hostname = FloatProperty
    Port = FloatProperty

    # constructor adding a component
    def __init__(self):
        print "SnServerComponent Pythoninit"
        self.deviceConfig = None;
        self.Hostname = "127.0.0.1"
        self.Port = 9998

    def debug(self, text):
        print "DEBUG", text, "HOST", self.Hostname, "PORT", self.Port

    def begin_play(self):

        #sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        with open('data.json') as data_file:
            self.deviceConfig = json.load(data_file)

        for sensorElement in self.deviceConfig["sensors"]:
        	sensorElement["sensorClass"] = SensorBase.getSensor(sensorElement["sensorType"], sensorElement["sensorParameters"])
        	#print sensorElement

        #print "Connecting, host=", self.Hostname, "port=", self.Port
        #
        #self.clientUDP = ClientUdp(self.Hostname, self.Port);
        #print self.deviceConfig;
        ##thread.start_new_thread(self.startClient, ())


    def connect(self):

        print "Connecting, host=", self.Hostname, "port=", self.Port

        self.clientUDP = ClientUdp(self.Hostname, self.Port);
        print self.deviceConfig;
        #thread.start_new_thread(self.startClient, ())


    def tick(self, delta_time):
        #print 'PythonTick'
        pass

    def startClient(self):

    	while True:
            for sensorElement in self.deviceConfig["sensors"]:
                sensor = sensorElement["sensorClass"]
                data = sensor.getData()
                sensorData = {'data': data, 'deviceId': self.deviceConfig["deviceId"], 'sensorId': sensorElement["sensorId"], 'sensorType': sensorElement["sensorType"], 'timestamp': (datetime.utcnow() - datetime(1970, 1, 1)).total_seconds()}
                print sensorData
                self.clientUDP.sendData(sensorData)

            time.sleep(1)


    def sendUeData(self, data):

        #ueSensor = SensorBase.getSensor("unrealengine", None)
        #data = ueSensor.getData()

        sensorData = {'data': data, 'deviceId': self.deviceConfig["deviceId"], 'sensorId': "testUE", 'sensorType': "unrealengine", 'timestamp': (datetime.utcnow() - datetime(1970, 1, 1)).total_seconds()}

        print "SENDING DATA:", json.dumps(sensorData).strip()
        self.clientUDP.sendData(sensorData)

        return True