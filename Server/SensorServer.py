from sensornetwork.DataStorage.DataStorageBase import DataStorageBase
from sensornetwork.Utils.Notification import Notification
from sensornetwork.Configuration.Configuration import Configuration
from sensornetwork.Communication.SensorUp import SensorUp
from sensornetwork.Communication.ClientUdp import ClientUdp
from datetime import datetime
import logging.config
import logging
import json
import time

#inicijalizacija parametara za stavaranje .log file-a
logging.config.fileConfig("logging.conf")
logger = logging.getLogger("SensorServer")
clientUdpForNotification = ClientUdp("192.168.1.6", 9998);

class SensorServer():
    dataStorage = None
    #polje u koje se spremaju poruke greske koje se zapisuju u log file
    errorMessages = []
    #varijabla koja predstavlja globalnu varijablu, error code = 1 znaci da je doslo do pogreske i ti se podaci zapisuju u log file
    ERROR_CODE_VALIDATION = 1
    sensorTypes = Configuration.loadConfiguration('sensorTypes.json')
    
    def __init__(self, notificationConditions):
        #prilikom stvaranja instance klase radi se konekcija na bazu
        #self.dataStorage = DataStorageBase(storageType, storageParameters)
        #prilikom stvaranja instance klase postavljaju se uvjeti za slanje notifikacija (slanje maila)
        self.notificationConditions = notificationConditions
    
    #komponenta koja omogucuje spajanje servera na bazu
    @classmethod
    def connectToDatabase(cls):
        #napravljena posebna komponenta za ucitavanje konfiguracije, ovdje ucitavamo podatke potrebne za konekciju na bazu
        dataFile = Configuration.loadConfiguration('databaseConnection.js')

        hostname = dataFile["hostname"]
        username = dataFile["username"]
        password = dataFile["password"]
        databaseName = dataFile["dbName"]
        storageType = dataFile["storageType"] 

        storageParameters = { "hostname":hostname, "user":username, "password":password, "dbName":databaseName}
        print "Connecting to database..."
        #napravljena instanca konekcije na bazu, tip oznauje da se radi o mysql bazi, kasnije mozemo impl drugacije konekcije
        SensorServer.dataStorage = DataStorageBase(storageType, storageParameters)

    #validacija podataka - provjerava imaju li primnjelni podaci sve potrebne atribute
    @classmethod
    def validateData(cls, data): 
        errorMessages = []
        result = True
        #svi podaci koji pristignu moraju sadrzavati atribute: deviceId, data, sensorId, timestamp, sensorType
        if(not "deviceId" in data or not data["deviceId"]):
            SensorServer.errorMessages.append("Missing required field deviceId''")
            result = False
        if(not "data" in data or not data["data"]):
            SensorServer.errorMessages.append("Missing required field data''")
            result = False
        if(not "sensorId" in data or not data["sensorId"]):
            SensorServer.errorMessages.append("Missing required field sensorId''")
            result = False
        if(not "timestamp" in data or not data["timestamp"]):
            SensorServer.errorMessages.append("Missing required field timestamp''")
            result = False
        if(not "sensorType" in data or not data["sensorType"]):
            SensorServer.errorMessages.append("Missing required field sensorType''")
            result = False
        if(not "sensorParameters" in data or not data["sensorParameters"]):
            SensorServer.errorMessages.append("Missing required field sensorParameters''")
            result = False
        #ako je validacija uspjesna vracamo true, inace false
        return result
            
    def handleData(self, inputData):
        sensorData = json.loads(inputData) #raspakiraj podatak
        print "\n" + "Data:", json.dumps(sensorData)
        print "Received data:" 
        if (SensorServer.validateData(sensorData)): #provjeri ima li podatak sva potrebna polja
            print " - passed data validation"
            response = json.dumps({'success':True})
            #samo ako se konekcija na bazu napravila dodaj podatke u bazu
            if (SensorServer.dataStorage):
                SensorServer.dataStorage.add(sensorData) #zapisi u bazu
            #condition = "DHT22"
            #self.dataStorage.read(condition) #citaj iz baze

            for elementName in sensorData['data']:
                testData = SensorUp.handleData(sensorData, sensorData['data'][elementName])
                #ova provjera je privremena jer nemamo sve datastreamove trenutno
                if testData['Datastream']['@iot.id'] != "None":
                    #print "Ovdje salje"
                    feedback = SensorUp.sendData(testData, "Observations")
                    print feedback

            self.notificationConditions = Configuration.loadConfiguration("notificationConditions.json")
            for conditionIndex in self.notificationConditions:
                if(Notification.checkCondition(sensorData, conditionIndex)):
                    #TODO: salji notifikaciju na mobitel
                    print " - passed check condition test"
                    #clientUdpForNotification.sendData(sensorData)
                    #ne koristimo slanje notifikacija za sada, ako necemo ni dalje ovaj dio koda moze se izbrisati
                    #exitcode, responseMessage, responseErrorMessage = Notification.sendNotification(json.dumps(sensorData["data"])) 
                    #print "Notification result: ", responseMessage + "\n" #ako je mail poslan output je Your e-mail has been sent, exitcode je 0
                    #if(responseErrorMessage):
                    #    print "Notification result, error: ", responseErrorMessage
                     #   logger.warning('%s', responseErrorMessage.rstrip('\n')) 
                #else:
                    #print("Check failed") #ovo je radi testiranja
            return response
        else:           
            response = json.dumps({'success':False, 'errorCode':SensorServer.ERROR_CODE_VALIDATION, 'errorMessages':SensorServer.errorMessages}) 
            #sve greske zapisuju se u log file
            logger.info("Sensor info: %s", json.dumps(sensorData))
            #["sensorId"], sensorData["deviceId"], sensorData["sensorType"]
            for errorMessage in range(len(SensorServer.errorMessages)):
                logger.warning('ErrorMessage: %s ------ ErrorCode: %s', SensorServer.errorMessages[errorMessage], SensorServer.ERROR_CODE_VALIDATION)
            print " - data validation failed"
            del SensorServer.errorMessages[:]        
            return response 



    