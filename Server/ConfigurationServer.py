from sensornetwork.Configuration.Configuration import Configuration
import json 
class ConfigurationServer():

    def handleData(self, configuration):
      datastreams = Configuration.loadConfiguration('datastreams.json')
      conditions =  Configuration.loadConfiguration('notificationConditions.json')
      data = Configuration.loadConfiguration('data.json')

      configuration = json.loads(configuration)
      datastreamIot = configuration["datastreamId"]
      for element in datastreams:
          if element["iot.id"] == datastreamIot:
              datastreamId = element["datastreamId"]

      for device in data:
          for sensor in device["sensors"]:
              for datastream in sensor["sensorParameters"]["datastreamId"]:
               	if sensor["sensorParameters"]["datastreamId"][datastream] == datastreamId:
               		observation = datastream
               		sensorId = sensor["sensorId"]
               		break

      for element in conditions:
       	if element["sensorId"] == sensorId:
       		element["dataCondition"][observation] = configuration["condition"]

      Configuration.saveConfiguration(conditions, 'notificationConditions')
      print "Configuration changed"

      response = json.dumps({'success': 'True'})
      return response




