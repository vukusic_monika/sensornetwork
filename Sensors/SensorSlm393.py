import json
from sensornetwork.Configuration.Configuration import Configuration

class SensorSlm393():

	#svaki senzor prima sensorParameters, ta varijabla sadrzi podatke o parametrima senzora, npr: pinovi na koje je prikljucen
    def __init__(self, sensorParameters):
    	self.sensorParameters = sensorParameters;

 	#dohvacanje podataka za senzor, svakki senzor ima drugacije podatke
    def getData(self):
        data = {"rad": "False"}
        return data
    
    #svaki senzor ima za sebe validaciju podataka koje je poslao i zadane uvijete prema kojima se validacija obavlja
    @classmethod
    def validateCondition(cls, sensorData, condition, observationName):
        print condition
        
        if(sensorData["data"][observationName] == condition["dataCondition"][observationName] and condition["dataCondition"][observationName]!="none"):
            notifications = Configuration.loadConfiguration('androidNotifications.json')
            datastreams = Configuration.loadConfiguration('datastreams.json')
            datastream = sensorData["sensorParameters"]["datastreamId"]
            for element in datastreams:
                if element["datastreamId"]==datastream:
                    datastreamIot = element["iot.id"]
            notifications["condition"] = "True"
            notifications["notifications"].append({
                "datastreamId": datastreamIot,
                "data": sensorData["data"][observationName] 
                })
            Configuration.saveConfiguration(notifications, 'androidNotifications')
            return True
        else:
            return False