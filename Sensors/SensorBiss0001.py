#import RPi.GPIO as GPIO
import time
import json
from sensornetwork.Configuration.Configuration import Configuration

class SensorBiss0001():

    #svaki senzor prima prilikom inicijalizacija sensorParameters, ta varijabla sadrzi podatke o parametrima senzora, npr: pinovi na koje je prikljucen
    def __init__(self, sensorParameters):
        self.sensorParameters = sensorParameters;

    #dohvacanje podataka za senzor, svakki senzor ima drugacije podatke
    def getData(self):

        #GPIO.setmode(GPIO.BCM)
        PIR_PIN = 4
        #GPIO.setup(PIR_PIN, GPIO.IN)

        #movement = GPIO.input(PIR_PIN) #treba li try?

        data = {"movement": movement}
        return data

    #svaki senzor ima za sebe validaciju podataka koje je poslao i zadane uvijete prema kojima se validacija obavlja
    @classmethod
    def validateCondition(cls, data, condition, observationName):
        movementCondition = condition["dataCondition"][observationName]
        if (data["data"][observationName]==movementCondition and movementCondition!= "none"):
            notifications = Configuration.loadConfiguration('androidNotifications.json')
            datastreams = Configuration.loadConfiguration('datastreams.json')
            datastream = data["sensorParameters"]["datastreamId"]
            for element in datastreams:
                if element["datastreamId"]==datastream:
                    datastreamIot = element["iot.id"]
            notifications["condition"] = "True"
            notifications["notifications"].append({
                "datastreamId": datastreamIot,
                "data": data["data"][observationName] 
                })
            Configuration.saveConfiguration(notifications, 'androidNotifications')
            return True
        else:
            return False