import json
from sensornetwork.Configuration.Configuration import Configuration

class SensorDht22():

    #svaki senzor prima prilikom inicijalizacija sensorParameters, ta varijabla sadrzi podatke o parametrima senzora, npr: pinovi na koje je prikljucen
    def __init__(self, sensorParameters):
        self.sensorParameters = sensorParameters;

    #dohvacanje podataka za senzor, svakki senzor ima drugacije podatke
    def getData(self):
        data = {"temperature": 12.54, "humidity": 0.45}
        #napraviti da odvojeno vraca temepraturu a odvojeno vlagu
        #data = {"temperature": 12.54}
        return data

    #svaki senzor ima za sebe validaciju podataka koje je poslao i zadane uvijete prema kojima se validacija obavlja
    @classmethod
    def validateCondition(cls, data, condition, observationName):


        if(condition["dataCondition"] == "none"):
            return False

        
        condition1 = condition["dataCondition"][observationName][0]
        condition2 = condition["dataCondition"][observationName][1]

        if ((data["data"][observationName]>condition1 and data["data"][observationName]<condition2) or (data["data"][observationName]>condition1 and condition2=="none") or (data["data"][observationName]<condition2 and condition1=="none")):
            notifications = Configuration.loadConfiguration('androidNotifications.json')
            datastreams = Configuration.loadConfiguration('datastreams.json')
            datastream = data["sensorParameters"]["datastreamId"]
            for element in datastreams:
                if element["datastreamId"]==datastream:
                    datastreamIot = element["iot.id"]
            notifications["condition"] = "True"
            notifications["notifications"].append({
                "datastreamId": datastreamIot,
                "data": data["data"][observationName] 
                })
            Configuration.saveConfiguration(notifications, 'androidNotifications')
            return True
        else:
            return False