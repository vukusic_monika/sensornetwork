from sensornetwork.Utils.SysInfo import SysInfo
import json
from sensornetwork.Configuration.Configuration import Configuration

class SensorRpi():

	#svaki senzor prima sensorParameters, ta varijabla sadrzi podatke o parametrima senzora, npr: pinovi na koje je prikljucen
    def __init__(self, sensorParameters):
    	self.sensorParameters = sensorParameters;

 	#dohvacanje podataka za senzor, svakki senzor ima drugacije podatke
    def getData(self):

        sysInfo = SysInfo()

        data = sysInfo.getSysInfo()
        return data
        
    #svaki senzor ima za sebe validaciju podataka koje je poslao i zadane uvijete prema kojima se validacija obavlja
    #@classmethod
    def validateCondition(data, condition, observationName):
        return