#!python
import SocketServer
import json
from sensornetwork.DataStorage.DataStorageBase import DataStorageBase
from sensornetwork.Server.SensorServer import SensorServer
        
class ServerUdp(SocketServer.BaseRequestHandler):

    lock = None
    sensorServer = None
    configurationCheckServer = None
    configurationServer = None
    onOffServer = None
    notificationServer = None
    newComponentServer = None
    #primanje podataka (koji se obraduju unutar SensorServer) i slanje odgovora
    def handle(self):
        self.lock.acquire()
        inputData = self.request[0].strip()
        
        inputData = json.loads(inputData)
        print inputData
        socket = self.request[1]
        if inputData["type"]=="sensor data":
            response = self.sensorServer.handleData(json.dumps(inputData))
        elif inputData["type"]=="configuration check":
        	response = self.configurationCheckServer.handleData(json.dumps(inputData))
        elif inputData["type"]=="notification check":
            response = self.notificationServer.handleData()
        elif inputData["type"]=="sensorOnOff":
            response = self.onOffServer.handleData(json.dumps(inputData))
        elif inputData["type"]=="change conditions":
            print inputData
            response = self.configurationServer.handleData(json.dumps(inputData))
        elif inputData["type"]=="new component":
            response = self.newComponentServer.handleData(json.dumps(inputData))
        #response = "OK"
        self.lock.release()
        socket.sendto(response, self.client_address)
        
        
#pristupanje stat.metodi: ServerUDP.validateData(data)